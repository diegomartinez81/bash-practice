cd ~/Downloads

git clone https://github.com/analogdevicesinc/libiio.git
cd libiio
cmake -DCMAKE_INSTALL_PREFIX=/usr .
make
sudo make install
cd ..

git clone https://github.com/analogdevicesinc/libad9361-iio.git
cd libad9361-iio
cmake -DCMAKE_INSTALL_PREFIX=/usr .
make
sudo make install
cd ..

git clone https://github.com/analogdevicesinc/gr-iio.git
cd gr-iio
cmake -DCMAKE_INSTALL_PREFIX=/usr .
make
sudo make install
cd ..
sudo ldconfig

cd Downloads
git clone git://github.com/BastilleResearch/gr-lora.git
cd gr-lora
mkdir build
cd build
cmake ../
make
sudo make install
sudo ldconfig


